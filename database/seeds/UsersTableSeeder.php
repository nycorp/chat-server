<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Yann Yvan",
            'email' => 'yann39@live.com',
            'password' => bcrypt('password'),
            'fcmToken' => ""
        ]);

        DB::table('users')->insert([
            'name' => "Lina Let",
            'email' => 'leticia.kegna@letsrealize.com',
            'password' => bcrypt('password'),
            'fcmToken' => ""
        ]);

        DB::table('users')->insert([
            'name' => "David Kalla",
            'email' => 'david.kalla@letsrealize.com',
            'password' => bcrypt('password'),
            'fcmToken' => ""
        ]);
    }
}
