<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Mobile\Auth\LoginController@login');

Route::get('test', function () {
    return response()->json([
        'status' => true,
        'message' => 'online',
    ]);
});

Route::middleware(\App\Http\Middleware\VerifyJWTToken::class)->group(function () {
    Route::get('logout', 'AuthController@logout');

    Route::post('create/deal', 'Mobile\Common\DealsController@create');
    Route::post('user/find', 'Mobile\GuestController@get_contacts');
    Route::post('user/update/fcm', 'Mobile\GuestController@update_fcm_Token');
    Route::post('create/message', 'Mobile\MessagesController@create');
    Route::post('message/read/notification', 'Mobile\MessagesController@notifyReadMessage');
});

