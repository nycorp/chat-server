<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Tymon\JWTAuth\Facades\JWTAuth;

class GuestController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        Config::set('jwt.user', 'App\Mobile\User');
        Config::set('auth.providers.users.model', \App\User::class);
    }

    public function update_fcm_Token(Request $request)
    {
        $this->user = JWTAuth::parseToken()->authenticate();
        $this->user->fcmToken = $request->fcmToken;
        if ($this->user->save()) {
            return response()->json(['status' => true,
                'message' => 1500]);
        }

        return response()->json(['status' => false,
            'message' => 1501]);

    }

    public function get_contacts()
    {
        $users = User::all();
        if (empty($users)) {
            return response()->json(['status' => false,
                'message' => 1501]);
        }
        return response()->json(['status' => true,
            'message' => 1500,
            'users' => $users]);
    }
}
