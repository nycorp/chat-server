<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Tymon\JWTAuth\Facades\JWTAuth;

class MessagesController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        Config::set('jwt.user', 'App\Mobile\User');
        Config::set('auth.providers.users.model', \App\User::class);
    }

    public function create(Request $request)
    {
        $data = json_decode($request->getContent());
        $message = new Message();
        $message->text = $data->message->text;
        $message->hiddenId = $data->message->hiddenId;
        $message->receiverId = $data->message->receiverId;
        $message->senderId = $data->message->senderId;
        $message->isSend = true;
        $message->message_type_id = 1;
        $message->date = date('Y-m-d H:i:s');
        $success = $message->save();
        $this->user = JWTAuth::parseToken()->authenticate();
        if (!$success)
            return response()->json(['status' => false, 'message' => 1501]);
        else {
            fcm()
                ->to([User::where('id', $message->receiverId)->first()->fcmToken])// $recipients must an array
                ->data([
                    'data' => ['title' => $this->user->name,
                        'message' => $message,]
                ])
                ->send();
            return response()->json(['status' => true, 'message' => 1500, 'bubble' => $message]);
        }
    }

    public function notifyReadMessage(Request $request)
    {
        $data = json_decode($request->getContent());
        $read_messages = array();
        foreach ($data->messages as $datum) {
            $message = new Message();
            $message->id = $datum->id;
            $message->hiddenId = $datum->hiddenId;
            $message->text = $datum->text;
            $message->hiddenId = $datum->hiddenId;
            $message->receiverId = $datum->receiverId;
            $message->senderId = $datum->senderId;
            $message->isSend = $datum->isSend;
            $message->message_type_id = 1;
            $message->isRead = true;
            $message->date = $datum->date;
            $message->update();
            array_push($read_messages, $message);
        }

        fcm()
            ->to([User::where('id', array_first($read_messages)->senderId)->first()->fcmToken])// $recipients must an array
            ->data([
                'data' => ['title' => '',
                    'context' => 'read_notification',
                    'messages' => $read_messages]
            ])
            ->send();
        return response()->json(['status' => true,
            'message' => 1500]);
    }
}
