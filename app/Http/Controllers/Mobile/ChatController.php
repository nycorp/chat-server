<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Config;

class ChatController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        Config::set('jwt.user', 'App\Mobile\User');
        Config::set('auth.providers.users.model', \App\User::class);
        $this->user = $user;
    }

    public function get_contacts()
    {
        $users = User::all();
        if (empty($this->user)) {
            return response()->json(['status' => false,
                'message' => 1501]);
        }
        return response()->json(['status' => true,
            'message' => 1500,
            'user' => $users]);
    }
}
