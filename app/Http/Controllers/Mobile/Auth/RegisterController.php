<?php

namespace App\Http\Controllers\Mobile\Auth;

use App\Http\Controllers\Controller;
use App\Mail\SendVerificationCode;
use App\User;
use App\UserVerifications;
use Dirape\Token\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $credentials = $request->only('name', 'email', 'password', 'role_id', 'phone');

        $rules = [
            'name' => 'required|max:255',
            'password' => 'required|max:32',
            'role_id' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|max:255|unique:users'
        ];

        $email_rule = [
            'email' => 'required|email|max:255|unique:users'
        ];

        $validator = Validator::make($credentials, $rules);

        if ($validator->fails()) {
            return Validator::make($credentials, $email_rule)->fails() ?
                response()->json(['status' => false, 'message' => 1101]) :
                response()->json(['status' => false, 'message' => 1104]);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone = $request->phone;
        $user->role_id = $request->role_id;
        $success = $user->save();

        self::sendVerificationCode($user->email);

        if (!$success)
            return response()->json(['status' => false, 'message' => 1103]);
        else
            return response()->json(['status' => true, 'message' => 1100]);
    }

    public static function sendVerificationCode($email)
    {
        $generator = new Token();

        //delete confirmation
        UserVerifications::where('user_email', $email)->delete();

        //generate verification code
        $code = $generator->UniqueNumber('user_verifications', 'code', 6);

        //generate token for this account
        $token = $generator->UniqueString('user_verifications', 'token', 30);

        //build verification link
        $url = url(action('Mobile\Auth\ConfirmationController@activateByToken', [$token]));

        //send email to the user address with all generated code below
        Mail::to($email)->send(new SendVerificationCode($code, $url));

        //save user generated code for account activation
        $confirmation_code = new UserVerifications;
        $confirmation_code->user_email = $email;
        $confirmation_code->code = $code;
        $confirmation_code->token = $token;
        $success = $confirmation_code->save();

        //check if the registration has succeed
        if (!$success) {
            return response()->json(['status' => false, 'message' => 1103]);
        }

        //return generated code for database savings
        return true;
    }

}
